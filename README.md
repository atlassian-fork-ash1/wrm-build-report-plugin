# WRM Build report webpack plugin

Build stats reporter for [atlassian webresource webpack plugin](https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin)

### Install

```
npm i -D @atlassian/atlassian-wrm-build-report-plugin
```

### Usage

```js
// webpack.config.js
const WrmBuildReportPlugin = require('@atlassian/atlassian-wrm-build-report-plugin');

module.exports = {
    plugins: [
      new WRMPlugin(...),
      new WrmBuildReportPlugin({
          filename: './report.json'
      })
    ]
};

```

Only works in conjunction with the [atlassian webresource webpack plugin](https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin).
Must be initialized after!

### API

##### filename

Type: `string`
Path to report file that will be generated.

##### externals

Type: `boolean`
Flag for adding external dependencies.
Calculates the size of external dependencies by requiring them from a running instance.
The url to that instance needs to be passed via `baseUrl`.

##### baseUrl

Type: `string`
URL to your service instance. Needed to calculate external sizes (if `externals` is `true`).
Needs to be the base URL to an instance serving WRM assets (e.g. Jira/Bitbucket/Confluence/Crowd etc...)

##### sizeOnly

Type: `boolean`
Default: `false`
Removes meta-data about external dependencies from the resulting stats file.

##### staticReport

Type: `boolean`
Default: `false`
Flag for generating static report.
Read more about [wrm dependencies analyzer](https://bitbucket.org/enrichmentcenter/atlassian-wrm-dependency-analyzer).
`baseUrl` needs to be specified for this to work correctly.

##### staticReportFilename

Type: `string`
Default: `static-report.html`
File name for static report.
