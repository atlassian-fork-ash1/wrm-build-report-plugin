const { calculateReport } = require('@atlassian/atlassian-wrm-external-dependencies-size-calculator');

function getExternalSize(chunkDescriptor, report) {
    if (!report) { 
        return {
            externalSize: 0,
            externalSizeJs: 0,
            externalSizeCss: 0,
        }
    }

    const totalSizeWithExternal = chunkDescriptor.size + report.externalSize.total;
    const totalSizeWithExternalJs = chunkDescriptor.sizeJs + report.externalSize.js;
    const totalSizeWithExternalCss = chunkDescriptor.sizeCss + report.externalSize.css;

    return {
        totalSize: totalSizeWithExternal,
        totalSizeJs: totalSizeWithExternalJs,
        totalSizeCss: totalSizeWithExternalCss,
        externalSize: report.externalSize.total,
        externalSizeJs: report.externalSize.js,
        externalSizeCss: report.externalSize.css,
    }
}

function getInheritedSize(chunkDescriptor, parentChunkDescriptor) {
    return {
        inheritedExternalSize: parentChunkDescriptor.externalSize,
        inheritedExternalSizeJs: parentChunkDescriptor.externalSizeJs,
        inheritedExternalSizeCss: parentChunkDescriptor.externalSizeCss,
        inheritedAndOwnExternalSize: chunkDescriptor.externalSize + parentChunkDescriptor.externalSize,
        inheritedAndOwnExternalSizeJs: chunkDescriptor.externalSizeJs + parentChunkDescriptor.externalSizeJs,
        inheritedAndOwnExternalSizeCss: chunkDescriptor.externalSizeCss + parentChunkDescriptor.externalSizeCss,
        totalInheritedAndOwnSize: chunkDescriptor.totalSize + parentChunkDescriptor.totalSize,
        totalInheritedAndOwnSizeJs: chunkDescriptor.totalSizeJs + parentChunkDescriptor.totalSizeJs,
        totalInheritedAndOwnSizeCss: chunkDescriptor.totalSizeCss + parentChunkDescriptor.totalSizeCss,
    }
}

function enrichChunkDescriptorWithExternal(chunkDescriptor, externalSizeReport) {
    return Object.assign({}, chunkDescriptor, getExternalSize(chunkDescriptor, externalSizeReport))
}

function enrichChunkDescriptorWithInheritedSize(chunkDescriptor, parentChunkDescriptor) {
    return Object.assign({}, chunkDescriptor, getInheritedSize(chunkDescriptor, parentChunkDescriptor))
}

function getExternalSizeReportForChunk(name, externalSizeReports) {
    return externalSizeReports.find( sizeReport => sizeReport.name === name);
}

module.exports = (buildStats, baseUrl) => calculateReport(buildStats, baseUrl).then(report => {
    return buildStats.map(entry => {
        const sizeReport = getExternalSizeReportForChunk(entry.name, report.externalSizes);
        const enrichedChunk = enrichChunkDescriptorWithExternal(entry, sizeReport);
        enrichedChunk.asyncChunks = entry.asyncChunks.map(asyncChunk => {
            const asyncSizeReport = getExternalSizeReportForChunk(asyncChunk.name, sizeReport.async);
            return enrichChunkDescriptorWithInheritedSize(
                enrichChunkDescriptorWithExternal(asyncChunk, asyncSizeReport),
                enrichedChunk
            );
        })
        
        return enrichedChunk;
    });
});